class AddPasswordDigestToAttendants < ActiveRecord::Migration[5.0]
  def change
    add_column :attendants, :password_digest, :string
  end
end
