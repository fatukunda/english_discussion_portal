class CreateAttendants < ActiveRecord::Migration[5.0]
  def change
    create_table :attendants do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :gender
      t.string :english_level
      t.text :joining_reason

      t.timestamps
    end
  end
end
