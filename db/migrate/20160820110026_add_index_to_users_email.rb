class AddIndexToUsersEmail < ActiveRecord::Migration[5.0]
  def change
  	 add_index :attendants, :email, unique: true
  end
end
