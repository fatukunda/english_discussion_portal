require 'test_helper'

class AttendantTest < ActiveSupport::TestCase
  def setup
    @Attendant = Attendant.new(first_name: "Example User",last_name: "Example user",
     email: "user@example.com" , password: "foobar",
       password_confirmation: "foobar", gender: "male",
      english_level: "Beginner")
  end

  test "should be valid" do
    assert @Attendant.valid?
  end
   test "First name should be present" do
    @Attendant.first_name = " "
    assert_not @Attendant.valid?
  end
  test "Last name should be present" do
  	@Attendant.last_name=""
  	assert_not @Attendant.valid?
  end
  test "email should be present" do
  	@Attendant.email=""
  	assert_not @Attendant.valid?
  end
  test "gender should be present" do
  	@Attendant.gender=""
  	assert_not @Attendant.valid?
  end
  test "english level should be present" do
  	@Attendant.english_level=""
  	assert_not @Attendant.valid?
  end

  #Length tests
  test "First name should not be too long" do
  	@Attendant.first_name= "a"*51
  	assert_not @Attendant.valid?
  end
  test "Last name should not be too long" do
  	@Attendant.last_name= "a"*51
  	assert_not @Attendant.valid?
  end
  test "email should not be too long" do
  	@Attendant.email="a" * 244 + "@example.com"
  	assert_not @Attendant.valid?
  end
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @Attendant.email = valid_address
      assert @Attendant.valid?, "#{valid_address.inspect} should be valid"
    end
  end
   test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @Attendant.email = invalid_address
      assert_not @Attendant.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  test "email addresses should be unique" do
    duplicate_attendant = @Attendant.dup
    duplicate_attendant.email = @Attendant.email.upcase
    @Attendant.save
    assert_not duplicate_attendant.valid?
  end
   test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @Attendant.email = mixed_case_email
    @Attendant.save
    assert_equal mixed_case_email.downcase, @Attendant.reload.email
  end
    test "password should be present (nonblank)" do
    @Attendant.password = @Attendant.password_confirmation = " " * 6
    assert_not @Attendant.valid?
  end
  test "password should have a minimum length" do
    @Attendant.password = @Attendant.password_confirmation = "a" * 5
    assert_not @Attendant.valid?
  end


end
