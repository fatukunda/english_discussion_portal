require 'test_helper'

class AttendantsSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'Attendant.count' do
      post attendants_path, params: { attendant: { first_name:  "",
      									 last_name: "",
      									 gender: "",
      									 english_level: "",
      									 joining_reason: "",
                                         email: "attendant@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'attendants/new'
    
  end
   test "valid signup information" do
    get signup_path
    assert_difference 'Attendant.count', 1 do
      post attendants_path, params: { attendant: { first_name:  "Example",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'attendants/show'
    assert is_logged_in?
  end
  
end
