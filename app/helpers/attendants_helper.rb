module AttendantsHelper
	 # Returns the Gravatar for the given Attendant.
  def gravatar_for(attendant, size: 80)
    gravatar_id = Digest::MD5::hexdigest(attendant.email.downcase)
    #size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/
    #{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: attendant.first_name + " " + attendant.last_name, class: "gravatar")
  end
end
