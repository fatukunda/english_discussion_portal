module SessionsHelper

  # Logs in the given user.
  def log_in(attendant)
    session[:attendant_id] = attendant.id
  end

  # Returns the current logged-in user (if any).
  def current_attendant
    @current_attendant ||= Attendant.find_by(id: session[:attendant_id])
  end
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_attendant.nil?
  end
  # Logs out the current user.
  def log_out
    session.delete(:attendant_id)
    @current_attendant = nil
  end
end