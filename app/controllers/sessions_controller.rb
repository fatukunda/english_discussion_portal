class SessionsController < ApplicationController

  def new
  end

  def create
    attendant = Attendant.find_by(email: params[:session][:email].downcase)
    if attendant && attendant.authenticate(params[:session][:password])
    	log_in attendant
    	redirect_to attendant
      # Log the user in and redirect to the user's show page.
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url

  end
end