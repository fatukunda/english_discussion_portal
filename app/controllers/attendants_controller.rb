class AttendantsController < ApplicationController
  def show
  	@attendant = Attendant.find(params[:id])
  end
  def new
  	@attendant=Attendant.new
  end
  def create
  	@attendant=Attendant.new(attendant_params)
  	if @attendant.save
      log_in @attendant
  		flash[:success] = "Welcome to the English discussion portal!"
  		redirect_to @attendant
  		
  	else
  		render 'new'
  		
  	end
  	
  end

  private
  def attendant_params
  	params.require(:attendant).permit(:first_name, :last_name,
  	 :gender, :english_level, :joining_reason, :email, 
  	 :password, :password_confirmation)
  	
  end
end
